#!/usr/bin/env bash

image=registry.gitlab.com/ytdocker/collection/urlwatch:localbuild

docker build --compress --tag ${image} .

docker run -it --rm \
	-v ${PWD}/dev:/root/.urlwatch \
	${image} \
	urlwatch --urls urls.yaml --config urlwatch.yaml --hooks hooks.py --cache cache.db

