#!/usr/bin/env bash

docker run -it --rm \
	-v ${PWD}/dev:/root/.urlwatch \
	registry.gitlab.com/ytdocker/collection/urlwatch \
	urlwatch --urls urls.yaml --config urlwatch.yaml --hooks hooks.py --cache cache.db

