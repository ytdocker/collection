#!/usr/bin/env bash

image=registry.gitlab.com/ytdocker/collection/signal:local

docker build --cache-from ${image} --tag ${image} .

export myUID=$(id -u)
export myGID=$(id -g)

#XAUTH=/tmp/.docker.xauth
#xauth nlist :0 | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -

	

#xhost +local:docker
docker run -it --rm						\
	--user $myUID:$myGID									\
	--env="DISPLAY"							\
	--workdir="/home/$USER" \
	--volume="/home/${USER}/alternativHome/signaltest:/home"	\
	--volume="/etc/group:/etc/group:ro"             	\
	--volume="/etc/passwd:/etc/passwd:ro"           	\
	--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw"     	\
    --volume=/dev/shm:/dev/shm \
	--device=/dev/snd:/dev/snd \
	--name signaltest \
	${image}

exit 0



	--entrypoint bash \
    -v /etc/machine-id:/etc/machine-id \
	--privileged \
	--volume="/etc/shadow:/etc/shadow:ro"           	\

	--volume="$XAUTH:$XAUTH" \

    -v /var/lib/dbus:/var/lib/dbus \
	--volume="/dev:/dev" \
    -v ~/.pulse:/home/$USER/.pulse \
    -v /run/user/$myUID/pulse:/run/user/$myUID/pulse \
	--volume="/etc/sudoers.d:/etc/sudoers.d:ro"     	\


