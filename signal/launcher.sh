#!/usr/bin/env bash

image=registry.gitlab.com/ytdocker/collection/signal:latest

docker pull ${image}

export myUID=$(id -u)
export myGID=$(id -g)


startit(){
	if [ "$1" == "of" ]; then
		myname=signalof
	else
		myname=signal
	fi

	docker run -d --rm						\
		--user $myUID:$myGID									\
		--env="DISPLAY"							\
		--workdir="/home/${USER}" \
		--volume="/home/${USER}/alternativHome/${myname}:/home/${USER}"	\
		--volume="/etc/group:/etc/group:ro"             	\
		--volume="/etc/passwd:/etc/passwd:ro"           	\
		--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw"     	\
        --volume=/dev/shm:/dev/shm \
        --device=/dev/snd:/dev/snd \
		--name ${myname} \
		--hostname ${myname} \
		${image}
}

startit
startit of
