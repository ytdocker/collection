#!/usr/bin/env bash
set -o errexit -o pipefail -o nounset

# locale
locale-gen ${MSEDGE_LOCALE}
update-locale LANG=${MSEDGE_LOCALE}

# timezone
if [ -f /etc/localtime ]; then rm /etc/localtime; fi
if [ -h /etc/localtime ]; then rm /etc/localtime; fi
ln -s /usr/share/zoneinfo/${TZ} /etc/localtime

USER_UID=${USER_UID:-1000}
USER_GID=${USER_GID:-1000}

MSEDGE_USER=msedgeuser

create_user() {
  # create group with USER_GID
  if ! getent group ${MSEDGE_USER} >/dev/null; then
    groupadd -f -g ${USER_GID} ${MSEDGE_USER} >/dev/null 2>&1
  fi

  # create user with USER_UID
  if ! getent passwd ${MSEDGE_USER} >/dev/null; then
    adduser --disabled-login --uid ${USER_UID} --gid ${USER_GID} \
      --gecos 'msedge' ${MSEDGE_USER} >/dev/null 2>&1
  fi
  #chown ${MSEDGE_USER}:${MSEDGE_USER} -R /home/${MSEDGE_USER}
  #adduser ${MSEDGE_USER} sudo
}

grant_access_to_video_devices() {
  for device in /dev/video*
  do
    if [[ -c $device ]]; then
      VIDEO_GID=$(stat -c %g $device)
      VIDEO_GROUP=$(stat -c %G $device)
      if [[ ${VIDEO_GROUP} == "UNKNOWN" ]]; then
        VIDEO_GROUP=msedgevideo
        groupadd -g ${VIDEO_GID} ${VIDEO_GROUP}
      fi
      usermod -a -G ${VIDEO_GROUP} ${MSEDGE_USER}
      break
    fi
  done
}

create_user
grant_access_to_video_devices




if [ "${1+x}" == "--debug" ]; then
	su -c "/usr/bin/xterm" ${MSEDGE_USER} &
    pids+=" $!"
else
  #teamsexe+=" --disable-namespace-sandbox --disable-setuid-sandbox"
  su -c "microsoft-edge --no-sandbox" ${MSEDGE_USER} &
  pids+=" $!"
fi

if [ "true" == "${START_XTERM}" ]; then 
	su -c "/usr/bin/xterm" ${MSEDGE_USER} &
    pids+=" $!"
fi
#if [ "true" == "${START_CHROME}" ]; then 
#	su -c "google-chrome --no-sandbox" ${MSEDGE_USER} &
#    pids+=" $!"
#fi



# Stop script
stop_script() {
	echo "killing progs ..."
    for pid in ${pids[*]}; do
        kill -SIGINT $pid
    done
	exit 0
}
# Wait for supervisor to stop script
trap stop_script SIGINT SIGTERM SIGKILL

echo "pids=${pids}"
for pid in ${pids[*]}; do
    wait $pid
done

