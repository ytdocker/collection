#!/usr/bin/env bash
set -o errexit -o pipefail -o nounset

image=registry.gitlab.com/ytdocker/collection/msedge:latest

docker pull ${image}

export myUID=$(id -u)
export myGID=$(id -g)


startit(){
	myname=msedge
	XAUTH=/tmp/.docker.${myname}.xauth
	touch ${XAUTH}
	xauth nlist :0 | sed -e 's/^..../ffff/' | xauth -f ${XAUTH} nmerge -

	docker run -it --rm	-d								\
		--env="DISPLAY=unix$DISPLAY"					\
		--env=USER_UID=${myUID}							\
		--env=USER_GID=${myGID}							\
		--env=XAUTHORITY=${XAUTH}						\
		--env=TZ=Europe/Berlin							\
		--volume="/home/${USER}/alternativHome/${myname}:/home/msedgeuser"	\
		--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw"		\
		--volume=${XAUTH}:${XAUTH}						\
		--volume=/run/user/${myUID}/pulse:/run/pulse	\
		--shm-size="1gb"								\
		--cpuset-cpus="0"								\
		--device /dev/dri								\
		--name ${myname}								\
		--hostname ${myname}							\
		${image}
#		--env=START_XTERM=true							\
#		--env=START_CHROME=true							\

#		--volume=/dev/shm:/dev/shm						\

}


startit
