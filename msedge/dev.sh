#!/usr/bin/env bash
set -o errexit -o pipefail -o nounset

image=registry.gitlab.com/ytdocker/collection/msedge:local
export DOCKER_BUILDKIT=0
docker build --cache-from ${image} --tag ${image} .

export myUID=$(id -u)
export myGID=$(id -g)

XAUTH=/tmp/.docker.xauth
touch ${XAUTH}
xauth nlist :0 | sed -e 's/^..../ffff/' | xauth -f ${XAUTH} nmerge -

#xhost +local:docker
docker run -it --rm									\
	--env="DISPLAY=unix$DISPLAY"					\
	--env=USER_UID=${myUID}							\
	--env=USER_GID=${myGID}							\
	--env=XAUTHORITY=${XAUTH}						\
	--env=TZ=Europe/Berlin							\
	--volume="/home/${USER}/alternativHome/msedge:/home/msedgeuser"	\
	--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw"		\
	--volume=${XAUTH}:${XAUTH}						\
	--volume=/run/user/${myUID}/pulse:/run/pulse	\
	--shm-size="1gb"								\
	--cpuset-cpus="0"								\
	--device /dev/dri								\
	--name msedge									\
	--hostname msedge								\
	--env=START_XTERM=false							\
	--env=START_CHROME=true							\
	${image} #--debug

# webcam
#	--device /dev/video*							\

#    --volume=/dev/shm:/dev/shm						\

exit 0
	--entrypoint xterm								\

	--user $myUID:$myGID							\
	--volume="/etc/group:/etc/group:ro"         	\
	--volume="/etc/passwd:/etc/passwd:ro"       	\


	--device=/dev/snd:/dev/snd					\

	--net=host \
	--volume="$HOME/.Xauthority:/home/${USER}/.Xauthority:rw" \
	--privileged \

    -v /var/lib/dbus:/var/lib/dbus \
    -v ~/.pulse:/home/$USER/.pulse \
    -v /run/user/$myUID/pulse:/run/user/$myUID/pulse \


    -v /etc/machine-id:/etc/machine-id \
	--volume="/etc/shadow:/etc/shadow:ro"           	\

	--volume="$XAUTH:$XAUTH" \

	--volume="/dev:/dev" \
	--volume="/etc/sudoers.d:/etc/sudoers.d:ro"     	\


