#!/usr/bin/env bash

image=registry.gitlab.com/ytdocker/collection/multimedia-tools:latest

docker build --cache-from ${image} --tag ${image} .

export myUID=$(id -u)
export myGID=$(id -g)
#export myUID=0
#export myGID=0

#XAUTH=/tmp/.docker.xauth
#xauth nlist :0 | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -

#xhost +local:docker

pathpipe=$(mktemp --dry-run)

#mkpipe(){
#	mkfifo ${pathpipe}
#	while read -r URL < ${pathpipe}; do xdg-open "$URL"; done	
#}
startit(){
	myname=multimedia-tools
	docker run -it --rm \
		--user $myUID:$myGID \
		--env="DISPLAY" \
		--volume="/etc/group:/etc/group:ro" \
		--volume="/etc/passwd:/etc/passwd:ro" \
		--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
		--device /dev/snd \
		--workdir="/worktemp" \
		--volume="./home:/home/${USER}:rw" \
		--volume="/home/user1/temp/musikextract:/worktemp:rw" \
		--name ${myname} \
		--hostname ${myname} \
		${image}
#		--volume="/home/${USER}/alternativHome/${myname}:/home/${USER}"	\
#		--volume="${pathpipe}:/tmp/xdg-open:rw" \
}

#mkpipe &
startit

#docker exec -it -u root ${myname} bash

#rm ${pathpipe}

exit 0


    --volume=/dev/shm:/dev/shm \
	--device=/dev/snd:/dev/snd \


	--entrypoint bash \
    -v /etc/machine-id:/etc/machine-id \
	--privileged \
	--volume="/etc/shadow:/etc/shadow:ro"           	\

	--volume="$XAUTH:$XAUTH" \

    -v /var/lib/dbus:/var/lib/dbus \
	--volume="/dev:/dev" \
    -v ~/.pulse:/home/$USER/.pulse \
    -v /run/user/$myUID/pulse:/run/user/$myUID/pulse \
	--volume="/etc/sudoers.d:/etc/sudoers.d:ro"     	\


