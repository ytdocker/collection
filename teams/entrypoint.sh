#!/usr/bin/env bash
set -e

# locale
locale-gen ${TEAMS_LOCALE}
update-locale LANG=${TEAMS_LOCALE}

# timezone
if [ -f /etc/localtime ]; then rm /etc/localtime; fi
if [ -h /etc/localtime ]; then rm /etc/localtime; fi
ln -s /usr/share/zoneinfo/${TZ} /etc/localtime

USER_UID=${USER_UID:-1000}
USER_GID=${USER_GID:-1000}

TEAMS_USER=teamsuser

create_user() {
  # create group with USER_GID
  if ! getent group ${TEAMS_USER} >/dev/null; then
    groupadd -f -g ${USER_GID} ${TEAMS_USER} >/dev/null 2>&1
  fi

  # create user with USER_UID
  if ! getent passwd ${TEAMS_USER} >/dev/null; then
    adduser --disabled-login --uid ${USER_UID} --gid ${USER_GID} \
      --gecos 'Teams' ${TEAMS_USER} >/dev/null 2>&1
  fi
  #chown ${TEAMS_USER}:${TEAMS_USER} -R /home/${TEAMS_USER}
  #adduser ${TEAMS_USER} sudo
}

grant_access_to_video_devices() {
  for device in /dev/video*
  do
    if [[ -c $device ]]; then
      VIDEO_GID=$(stat -c %g $device)
      VIDEO_GROUP=$(stat -c %G $device)
      if [[ ${VIDEO_GROUP} == "UNKNOWN" ]]; then
        VIDEO_GROUP=teamsvideo
        groupadd -g ${VIDEO_GID} ${VIDEO_GROUP}
      fi
      usermod -a -G ${VIDEO_GROUP} ${TEAMS_USER}
      break
    fi
  done
}

create_user
grant_access_to_video_devices


#cd /home/${TEAMS_USER}
#su -c "gio mime x-scheme-handler/https org.mozilla.firefox.desktop" ${TEAMS_USER}
#su -c "gio mime x-scheme-handler/http org.mozilla.firefox.desktop" ${TEAMS_USER}


if [ "$1" == "--debug" ]; then
	su -c "/usr/bin/xterm" ${TEAMS_USER} &
    pids+=" $!"
else
	if [ "true" == "${TEAMS_INSIDERS}" ]; then 
        teamsexe="/usr/share/teams-insiders/teams-insiders"
        logpath="/home/${TEAMS_USER}/.config/Microsoft/Microsoft Teams - Insiders"
	else
        teamsexe="/usr/share/teams/teams"
        logpath="/home/${TEAMS_USER}/.config/Microsoft/Microsoft Teams"
	fi
    teamsexe+=" --disable-namespace-sandbox --disable-setuid-sandbox"
    su -c "${teamsexe}" ${TEAMS_USER} &
    pids+=" $!"
fi

if [ "true" == "${START_XTERM}" ]; then 
	su -c "/usr/bin/xterm" ${TEAMS_USER} &
    pids+=" $!"
fi
if [ "true" == "${START_CHROME}" ]; then 
	su -c "google-chrome --no-sandbox" ${TEAMS_USER} &
    pids+=" $!"
fi


log_2stdout() {
	sleep 5
	tail -f "${logpath}/logs.txt" &
	tail -f "${logpath}/logs/teams-startup.log" &
}
log_2stdout &


# Stop script
stop_script() {
	echo "killing progs ..."
    for pid in ${pids[*]}; do
        kill -SIGINT $pid
    done
	exit 0
}
# Wait for supervisor to stop script
trap stop_script SIGINT SIGTERM SIGKILL

echo "pids=${pids}"
for pid in ${pids[*]}; do
    wait $pid
done

