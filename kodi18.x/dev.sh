#!/usr/bin/env bash

image=registry.gitlab.com/ytdocker/collection/kodi18.x:latest
image=registry.gitlab.com/ytdocker/collection/kodi18.x:local

#docker build --cache-from ${image} --tag ${image} .

export myUID=$(id -u)
export myGID=$(id -g)

#XAUTH=/tmp/.docker.xauth
#xauth nlist :0 | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -

docker build -t ${image} .
	

xhost +local:docker
docker run -it --rm						\
	--user $myUID:$myGID									\
	--env="DISPLAY"							\
	--workdir="/home/$USER" \
	--volume="/home/$USER/alternativHome/kodi/:/home/$USER"	\
	--volume="/etc/group:/etc/group:ro"             	\
	--volume="/etc/passwd:/etc/passwd:ro"           	\
	--volume="/etc/shadow:/etc/shadow:ro"           	\
	--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw"     	\
    -v /dev/shm:/dev/shm \
    -v /etc/machine-id:/etc/machine-id \
	-v /dev/snd:/dev/snd \
	-v /var/run/dbus/system_bus_socket:/var/run/dbus/system_bus_socket \
	--name kodi \
	--privileged \
	--entrypoint bash \
	${image}

exit 0

	--volume="$XAUTH:$XAUTH" \

    -v /var/lib/dbus:/var/lib/dbus \
	--volume="/dev:/dev" \
    -v ~/.pulse:/home/$USER/.pulse \
    -v /run/user/$myUID/pulse:/run/user/$myUID/pulse \
	--volume="/etc/sudoers.d:/etc/sudoers.d:ro"     	\

