#!/usr/bin/env bash
set -o errexit -o pipefail -o nounset

cp /workdir/authorized_keys /root/.ssh/
chown root:root /root/.ssh/authorized_keys
chmod 600 /root/.ssh/authorized_keys

# Generate Host keys, if required
if ! ls /etc/ssh/ssh_host_* 1> /dev/null 2>&1; then
    echo "generate host keys"
    /usr/bin/ssh-keygen -A
fi

/usr/sbin/sshd -D -e -f /workdir/sshd_config &

echo "running for ${LIFETIME}"
sleep ${LIFETIME}
echo "time is up. Bye ..."
