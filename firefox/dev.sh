#!/usr/bin/env bash
set -o errexit -o pipefail -o nounset

image=registry.gitlab.com/ytdocker/collection/firefox:local
export DOCKER_BUILDKIT=0
docker build --cache-from ${image} --tag ${image} .

export myUID=$(id -u)
export myGID=$(id -g)

XAUTH=/tmp/.docker.xauth
touch ${XAUTH}
xauth nlist :0 | sed -e 's/^..../ffff/' | xauth -f ${XAUTH} nmerge -


startit(){
	myname=firefox
	docker run  --rm \
		--user $myUID:$myGID \
		--volume="/etc/group:/etc/group:ro" \
		--volume="/etc/passwd:/etc/passwd:ro" \
		--env="DISPLAY=unix$DISPLAY"					\
		--env=USER_UID=${myUID}							\
		--env=USER_GID=${myGID}							\
		--env=XAUTHORITY=${XAUTH}						\
		--env=TZ=Europe/Berlin							\
		--workdir="/home/${USER}" \
		--volume="./${myname}:/home/${USER}"	\
		--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
		--volume=${XAUTH}:${XAUTH}						\
		--volume=/run/user/${myUID}/pulse:/run/pulse	\
		--device /dev/dri								\
		--name ${myname} \
		--hostname ${myname} \
		${image}
}

startit

exit 0
		--volume="/home/${USER}/alternativHome/${myname}:/home/${USER}"	\
