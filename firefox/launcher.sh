#!/usr/bin/env bash
set -o errexit -o pipefail -o nounset

image=registry.gitlab.com/ytdocker/collection/firefox:latest

docker pull ${image}

export myUID=$(id -u)
export myGID=$(id -g)


startit(){
	myname=firefox
	XAUTH=/tmp/.docker.${myname}.xauth
	touch ${XAUTH}
	xauth nlist :0 | sed -e 's/^..../ffff/' | xauth -f ${XAUTH} nmerge -
	docker run --rm \
		--user $myUID:$myGID \
		--volume="/etc/group:/etc/group:ro" \
		--volume="/etc/passwd:/etc/passwd:ro" \
		--env="DISPLAY=unix$DISPLAY"					\
		--env=USER_UID=${myUID}							\
		--env=USER_GID=${myGID}							\
		--env=XAUTHORITY=${XAUTH}						\
		--env=TZ=Europe/Berlin							\
		--workdir="/home/${USER}" \
		--volume="/home/${USER}/alternativHome/${myname}:/home/${USER}"	\
		--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
		--volume=${XAUTH}:${XAUTH}						\
		--volume=/run/user/${myUID}/pulse:/run/pulse	\
		--device /dev/dri								\
		--name ${myname} \
		--hostname ${myname} \
		${image}
}

startit

