# docker-nas-sshd-helper

            '/root/.ssh/authorized_keys:/workdir/authorized_keys:ro',
            '/etc/ssh/ssh_host_dsa_key:/etc/ssh/ssh_host_dsa_key:ro',
            '/etc/ssh/ssh_host_ecdsa_key.pub:/etc/ssh/ssh_host_ecdsa_key.pub:ro',
            '/etc/ssh/ssh_host_ecdsa_key:/etc/ssh/ssh_host_ecdsa_key:ro',
            '/etc/ssh/ssh_host_dsa_key.pub:/etc/ssh/ssh_host_dsa_key.pub:ro',
            '/etc/ssh/ssh_host_ed25519_key:/etc/ssh/ssh_host_ed25519_key:ro',
            '/etc/ssh/ssh_host_rsa_key:/etc/ssh/ssh_host_rsa_key:ro',
            '/etc/ssh/ssh_host_ed25519_key.pub:/etc/ssh/ssh_host_ed25519_key.pub:ro',
            '/etc/ssh/ssh_host_rsa_key.pub:/etc/ssh/ssh_host_rsa_key.pub:ro'
