#!/usr/bin/env bash
set -o errexit -o pipefail -o nounset


if ls ${SSHKEYS}/ssh_host_* 1> /dev/null 2>&1; then
  cp -f ${SSHKEYS}/ssh_host_* /etc/ssh/
  chown -R 0:0 /etc/ssh
  cd /etc/ssh
  chmod 600 *_key
  chmod 644 *.pub
fi


# Generate Host keys, if required
if ! ls /etc/ssh/ssh_host_* 1> /dev/null 2>&1; then
  echo "generate host keys"
  ssh-keygen -A
fi

mkdir -p /root/.ssh
cp -f ${SSHKEYS}/authorized_keys /root/.ssh/authorized_keys
chmod 600 /root/.ssh/authorized_keys

/usr/sbin/sshd -D -e -f /etc/ssh/sshd_config
