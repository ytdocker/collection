#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail

fullname=$1
name=${fullname%%-*}
version=${fullname#-*}

echo "setup extension: $name  in version: $version"

wget https://extdist.wmflabs.org/dist/extensions/${fullname} -O temp.tar.gz && \
    tar -xzf temp.tar.gz -C /var/www/html/extensions && \
    rm temp.tar.gz && \
    cd /var/www/html/extensions/${name} && composer install

