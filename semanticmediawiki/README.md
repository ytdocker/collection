# Links
https://hub.docker.com/_/mediawiki?tab=description&page=1&ordering=last_updated

* https://gitlab.com/hcc23/semantic-mediawiki-docker/-/tree/master
* https://gitlab.com/pleio/semantic-mediawiki/-/blob/master/Dockerfile
* https://www.semantic-mediawiki.org/
* https://www.mediawiki.org/wiki/Extension:Semantic_Drilldown
* https://packagist.org/packages/mediawiki/
* https://extdist.wmflabs.org/dist/extensions/

# neuer OAuth-Client?
* https://www.mediawiki.org/wiki/Extension:OpenID_Connect


# Vorgehensweise Update
* sich für eine Wiki-Version entscheiden und als Tag in die FROM Zeile
* die für die Version vorgesehenen Extension-Snapshot Namen besorgen von: https://www.mediawiki.org/wiki/Special:ExtensionDistributor  
  und ins Dockerfile notieren.
* das eigene Image mit der Wiki-Version taggen (u.a.)


