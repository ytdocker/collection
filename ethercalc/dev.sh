#!/usr/bin/env bash

imagename=temp

docker build -t ${imagename} .
docker run -it --rm --name temp -p 8000:8000  ${imagename}
