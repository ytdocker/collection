#!/usr/bin/env bash

imagename=temp

docker build --build-arg ETHERPAD_VERSION=v2.0.1 -t ${imagename} .
docker run -it --rm --name temp -p 9001:9001  ${imagename}
