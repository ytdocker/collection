#!/usr/bin/env python3

import os, sys, time, shutil
import subprocess
import functools
from argparse import ArgumentParser
import logging



def main():
    myret = 0    
    realPackages = cli.packages
    if len(realPackages) == 0:
        logger.info("got no packages, build all")
        realPackages = [f.name for f in os.scandir('.') if f.is_dir() and not (f.name.startswith('.') or f.name == "log")]
    for pack in realPackages:
        logger.info(f"{10*' '} {(len(pack)+4)*'_'}")
        logger.info(f"{10*'_'}[_ {pack} _] start {25*'_'}")
        try:
            ret = buildimage(pack, cacheSelf=False)
        except Exception as err:
            logger.error(err)
            ret = 1
        logger.info(f"{10*'='}[_ {pack} _] retcode={ret}  {20*'='}")
        myret = ret if ret else myret
    return myret

def buildimage(package, cacheSelf=True):
    if os.path.isfile(f"{package}/disable"):
        logger.info(f"{package} is disabled, exit")
        return 0

    logpath = f"log/{package}.log"
    if os.path.exists(logpath) and not cli.dryrun:
        os.remove(logpath)

    image = "/".join([cli.regbasepath, package]).lower()
    logger.info(f"image = {image}")

    cachefrom=""
    if cacheSelf:
        rundockercli("pull", f"{image}:latest", logpath)
        cachefrom=f"--cache-from {image}:latest"

    realtags = cli.tags[:] + ["latest"]
    # read additional tags
    tagexe = os.path.join(package, "buildtag.sh")
    if os.path.exists(tagexe):
        realtags += subprocess.run([tagexe], stdout=subprocess.PIPE).stdout.rstrip().decode('utf-8').split()
    tags = "".join([f"-t {image}:{t} " for t in realtags ])

    # arg
    arg = ""
    argexe = os.path.join(package, "buildarg.sh")
    if os.path.exists(argexe):
        arg = subprocess.run([argexe], stdout=subprocess.PIPE).stdout.rstrip().decode('utf-8')
    ret = rundockercli("build", f"--pull --compress {cachefrom} {tags} {arg} {package}/.", logpath)
        
    if ret == 0 and not cli.nopush:
        for tag in realtags:
            r = rundockercli("push", f"{image}:{tag}", logpath)
            ret = r if r else ret
    return ret

def rundockercli(verb, args, logpath):
    ret = 0
    def wlf(string): # writelogfile
        shell.run(f"echo '{string}' >> {logpath}", raiseerror=False)
    if not cli.dryrun:
        cmd = f"docker {verb} {args}"
        wlf(f"==========================   {verb}   ==========================")
        wlf(f"cmd: {cmd}")
        wlf( "----------------------------------------------------------------")
        ret = shell.run(f"{cmd} >> {logpath} 2>&1", raiseerror=False)
    else:
        logger.warning(f"dryrun: docker {verb} {args}")
    return ret

#  ------- ↓↓↓↓↓↓↓ -------  helper  ------- ↓↓↓↓↓↓↓ -------
def init():
    global logger
    global cli
    logging.basicConfig(
        format='[%(asctime)s][%(name)s] %(levelname)s: %(message)s',
        level=logging.INFO,
        handlers=[logging.StreamHandler(sys.stdout)]
    )
    logger = logging.getLogger(__name__)

    parser = ArgumentParser(description="build some images", add_help=False)
    group = parser.add_argument_group("required arguments")
    group.add_argument("packages", nargs='*')
    group.add_argument("-t", "--tag",   dest="tags", action="append", help="Tags, can be entered multiple times", required=True)
    group.add_argument("--regbasepath", dest="regbasepath",  help="basepath of registry", required=True)

    group = parser.add_argument_group("optional arguments")
    group.add_argument("-n", "--nopush",  action="store_true", dest="nopush",  help="do not push images to registry")
    group.add_argument("-d", "--dryrun",  action="store_true", dest="dryrun",  help="only print the action")
    group.add_argument("-v", "--verbose", action="store_true", dest="verbose", help="set loglevel to debug")
    group.add_argument("-h", "--help",    action="help", help="show this help message")
    cli = parser.parse_args()
    if cli.verbose:
        logging.getLogger().setLevel(logging.DEBUG)

class shell:
    class decor:
        @staticmethod
        def __funcfullname(func, *args, **kwargs):
            args_repr = [repr(a) for a in args]                      # 1
            kwargs_repr = [f"{k}={v!r}" for k, v in kwargs.items()]  # 2
            signature = ", ".join(args_repr + kwargs_repr)           # 3
            return f"{func.__name__}({signature})"

        @classmethod
        def debug(cls, func):
            """Print the function signature and return value"""
            @functools.wraps(func)
            def wrapper(*args, **kwargs):
                logger.debug(f"Calling {cls.__funcfullname(func, *args, **kwargs)}")
                value = func(*args, **kwargs)
                logger.debug(f"{func.__name__!r} returned {value!r}")           # 4
                return value
            return wrapper

        @classmethod
        def dryrun(cls, func):
            @functools.wraps(func)
            def wrapper(*args, **kwargs):
                if cli.dryrun:
                    logger.info(f"dryrun: Calling {cls.__funcfullname(func, *args, **kwargs)}")
                else:
                    return func(*args, **kwargs)
            return wrapper


    @staticmethod
    @decor.debug
    @decor.dryrun
    def mkdir(directory, exist_ok=True):
        return os.makedirs(directory, exist_ok=exist_ok)
        
    @staticmethod
    @decor.debug
    @decor.dryrun
    def rmdir(directory):
        return os.rmdir(directory)
        
    @staticmethod
    @decor.debug
    @decor.dryrun
    def mv(src, dest):
        return shutil.move(src, dest)

    @staticmethod
    @decor.debug
    @decor.dryrun
    def run(cmd, raiseerror=True):
        ret = os.system(f"/bin/bash -c \"set -o pipefail; {cmd}\"")
        # wenn keine bash benötigt wird:
        #ret = os.system(cmd)
        if ret != 0:
            errtext=f"run: returncode={ret}  in: {cmd}"
            logger.error(errtext)
            if raiseerror:
                raise Exception(errtext)
        return ret


init()

if __name__ == "__main__":
    # execute only if run as a script
    try:
        logger.info(f"start  cli: {cli}")
        ret = main()
        logger.info(f"stop with returncode={ret}")
        if ret > 255:
            logger.info(f"returncode > 255 return now 1")
            sys.exit(1)
        sys.exit(ret)
    except Exception as err:
        logger.error(err)
        raise(err)
else:
    pass
    
