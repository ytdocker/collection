#!/usr/bin/env bash

image=registry.gitlab.com/ytdocker/collection/scanservjs-epson:latest

docker build --cache-from ${image} --tag ${image} .

startit(){
	myname=scanservjs-epson
	docker run  --rm \
		--name ${myname} \
		--hostname ${myname} \
		-e TZ=Europe/Berlin \
		${image}
}

startit

