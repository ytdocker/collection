#!/usr/bin/env bash
set -o errexit -o pipefail -o nounset

mkdir -p /root/.ssh /run/sshd
cp /authorized_keys /root/.ssh/
chown root:root /root/.ssh/authorized_keys
chmod 600 /root/.ssh/authorized_keys

# Generate Host keys, if required
if ! ls /etc/ssh/ssh_host_* 1> /dev/null 2>&1; then
    echo "generate host keys"
    /usr/bin/ssh-keygen -A
fi

/usr/sbin/sshd -D -e -f /etc/ssh/sshd_config
