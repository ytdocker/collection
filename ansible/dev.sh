#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail

mypath=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

imagetag=registry.gitlab.com/ytdocker/collection/ansible
containername=ansible
port=2022

#####################
#  BUID
docker build -t ${imagetag} ${mypath}


#####################
#  USAGE
docker run -it --rm -d -p ${port}:22 \
	-v ~/.ssh/id_rsa.pub:/authorized_keys:ro \
	--name ${containername} ${imagetag} 

sleep 1
ssh -A -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null \
    -p ${port} root@localhost \
    -t 'bash -l -c "cd /tmp;bash"'

docker stop ${containername} > /dev/null

